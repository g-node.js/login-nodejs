'use strict'
const express = require('express');
const cors = require('cors');

const authRoutes = require('./auth/auth.routes');
const properties = require('./config/properties');
const DB = require('./config/db');

// init DB. Arranca la base de datos
DB();
const app = express();
const router = express.Router();

const bodyParser = require('body-parser');
const bodyParserJSON = bodyParser.json();
const bodyParserURLEncoded = bodyParser.urlencoded({extended: true});

// support parsing of application/json type post data
app.use(bodyParserJSON);
// support parsing of application/x-www-form-urlencoded post data
app.use(bodyParserURLEncoded);

authRoutes(router);
// Se habilitan todas las peticiones al server, pero se podria evitar dominios
app.use(cors());
app.use('/api', router);
// Ruta para el home
router.get('/', (req, res) => {
    res.send('Hello from home');
});
app.use(router);
app.listen(properties.PORT, () => console.log(`server running on port ${properties.PORT} using Nodemon (Nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected)`));