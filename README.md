# login-backend

Registro e inicio de sesión con Node.js, Express y MongoDB.

## Conceptos a desarrollar

API-REST, API pública, Protocolo HTTP, login, jwt, token, tiempo de expiración  

## Tecnologías

Lenguaje: Javascript

Framework: Express

Entorno de ejecución: Node.js

Gestor de paquetes: npm

Gestor de Base de Datos: MongoDB

Prueba de la API: Postman (Opcional)

## Uso
Para levantar el servidor ejecutar el siguiente comando:

```bash
1) npm run start (esto ejecuta nodemon server.js)
```

## URIs
POST --> /register

```bash
{
	"email": "test@gmail.com",
	"name": "test",
	"password": "123456"
}
```

POST --> /login

```bash
{
	"email": "test@gmail.com",
	"password": "123456"
}
```

## Cliente

## Referencias
[Domini Code](https://www.youtube.com/watch?v=KyyzhRqkJRA&t=179s)