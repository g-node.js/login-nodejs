const mongoose = require('mongoose');
const dbURL = require('./properties').DB;

module.exports = () => {
    // The connect() function also accepts a callback parameter and returns a promise.
    mongoose.connect(dbURL, {useNewUrlParser: true}).then( 
        ()    => console.log(`Mongo connect on ${dbURL}`),
        err => console.log(`Connection has error ${err}`))

    // El objeto de proceso es un global que proporciona información y control sobre el proceso actual de Node.js
    // Signal events will be emitted when the Node.js process receives a signal.
    // Please refer to signal(7) for a listing of standard POSIX signal names such as 'SIGINT', 'SIGHUP', etc.
    // Signal      Standard   Action   Comment
    // SIGINT       P1990      Term    Interrupt from keyboard
    process.on('SIGINT', () => {
        mongoose.connection.close(() => {
            console.log(`Mongo is disconnected`);
            process.exit(0);
        })
    })
}