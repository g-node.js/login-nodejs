const authSchema = require('./auth.model');
const mongoose = require('mongoose');

/**
 * Se añaden funciones estaticas al modelo
 */
authSchema.statics = {
    create: function(data, cb) {
        const user = new this(data);
        user.save(cb);
    },
    login: function(query, cb) {
        this.find(query, cb);
    }
}

const authModel = mongoose.model('Users', authSchema);
module.exports = authModel;