/**
 * Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment.
 * Mongoose supports both promises and callbacks.
 */
const mongoose = require('mongoose');

/**
 * Se define el modelo de datos para la coleccion
 * Models are defined through the Schema interface
 */
const Schema = mongoose.Schema;
mongoose.set('useCreateIndex', true);

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }
}, {
    timestamps: true
});
// el timestap guarda la fecha de creacion y la fecha de actualizacion

module.exports = userSchema;