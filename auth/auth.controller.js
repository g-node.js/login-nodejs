const User = require('./auth.dao');
/**
 * Para la creación de tokens de acceso
 * Para crear un token que sirva para enviar datos entre aplicaciones o servicios y
 * garantizar que sean válidos y seguros.
 * El caso más común de uso de los JWT es para manejar la autenticación en aplicaciones móviles o web.
 * Para esto cuando el usuario se quiere autenticar manda sus datos de inicio del sesión al servidor,
 * este genera el JWT y se lo manda a la aplicación cliente, luego en cada petición el cliente envía
 * este token que el servidor usa para verificar que el usuario este correctamente autenticado
 * y saber quien es.
 */
const jwt = require('jsonwebtoken');
/**
 * bcryptjs es una biblioteca optimizada escrita en javascript que permite trabajar con 
 * hash bcrypt de forma simple y eficiente.
 */
const bcrypt = require('bcryptjs');
const SECRET_KEY = 'secretkey123456';

/**
 * Metodo para crear un nuevo usuario
 * Middleware: next
 */
exports.createUser = (req, res, next) => {
    const newUser = {
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password)
    }
    /**
     * create: metodo que guarda los datos del usuario
     * newUser: usuario a guardar
     * callback
     */
    User.create(newUser, (err, user) => {
        if (err && err.code === 11000) return res.status(409).send('Email already exists');
        if (err) return res.status(500).send('Server error');
        const expiresIn = 24 * 60 * 60;
        /**
         * jwt.sign(payload, secretOrPrivateKey, [options, callback])
         * payload could be an object literal, buffer or string representing valid JSON.
         */
        const accessToken = jwt.sign(
            {id: user.id}, 
            SECRET_KEY, 
            {expiresIn: expiresIn}
        );
        const dataUser = {
            name: user.name,
            email: user.email,
            accessToken: accessToken,
            expiresIn: expiresIn
        }
        // Respuesta al front-end
        res.send({dataUser});
    })
}

exports.loginUser = (req, res, next) => {
    const userData = {
        email: req.body.email,
        password: req.body.password,
    }
    User.findOne({email: userData.email}, (err, user) => {
        if (err) return res.status(500).send('Server error!');
        if (!user) {
            // Si no recupera ningun usuario, el email no existe
            return res.status(409).send({message: 'Something is wrong'})
        } else {
            // Hay que descodificar el password
            // userData viene del frontend y user es lo que se recupero de la base de datos
            const resultPassword = bcrypt.compareSync(userData.password, user.password);
            // Si coincide el password desencriptado con el que pasaron el usuario existe
            if (resultPassword) {
                const expiresIn = 24 * 60 * 60;
                /**
                 * Hay que devolverlo hacia el front-end
                 */
                const accessToken = jwt.sign(
                    {id: user.id}, 
                    SECRET_KEY, 
                    {expiresIn: expiresIn}
                );
                const dataUser = {
                    name: user.name,
                    email: user.email,
                    accessToken: accessToken,
                    expiresIn: expiresIn
                }        
                res.send({dataUser});
            } else {
                // password wrong
                res.status(409).send({message: 'Something is wrong'})
            }
        }
    });

}